#include "Particle.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include <fstream>
#include <TROOT.h>
#include <TStyle.h>
#include <string>
#include <iostream>

Particle::Particle( std::string configFileName ) {

    _pdgID = 11;

    double px = 0.;
    double py = 0.;
    double pz = 0.;
    double E = 0.;
    double x = 0.;
    double y = 0.;
    double z = 0.;

    std::ifstream configFile( configFileName.c_str(), std::ios::in );
    while( configFile.good() ) {
      std::string key, value;
      configFile >> key >> std::ws >> value;
      if( configFile.eof() ) break;
      if( key == "PX"             )  px = atof(value.c_str()); 
      if( key == "PY"             )  py = atof(value.c_str()); 
      if( key == "PZ"             )  pz = atof(value.c_str()); 
      if( key == "E"              )  E = atof(value.c_str()); 
      if( key == "PDGID"          )  _pdgID = atoi(value.c_str());
      if( key == "X"              )  x = atof(value.c_str()); 
      if( key == "Y"              )  y = atof(value.c_str()); 
      if( key == "Z"              )  z = atof(value.c_str()); 
    }

    _FourMomentum.SetPxPyPzE( px, py, pz, E );
    _Vertex.SetXYZ( x, y, z );
    _FourMomentumHistory.push_back( _FourMomentum );
    _VertexHistory.push_back( _Vertex );
  
}

Particle::Particle() {
    
    _pdgID = 11;

  
    _FourMomentum.SetPxPyPzE( 0., 0., 0., 0.);
    _Vertex.SetXYZ( 0., 0., 0. );
    _FourMomentumHistory.push_back( _FourMomentum );
    _VertexHistory.push_back( _Vertex );
    
}

TLorentzVector Particle::GetFourMomentum() {

  return _FourMomentum;

}

TVector3 Particle::GetVertex() {

  return _Vertex;

}

std::vector<TLorentzVector> Particle::GetFourMomentumHistory() {

  return _FourMomentumHistory;

}

std::vector<TVector3> Particle::GetVertexHistory() {

  return _VertexHistory;

}

void Particle::SetFourMomentum( TLorentzVector newMomentum ) {

  _FourMomentum.SetPxPyPzE( newMomentum.Px(), newMomentum.Py(), newMomentum.Pz(), newMomentum.E() );

}

void Particle::SetVertex( TVector3 newVertex ) {

  _Vertex.SetXYZ( newVertex.X(), newVertex.Y(), newVertex.Z() );

}

void Particle::SetFourMomentumVertex( TLorentzVector newMomentum, TVector3 newVertex ) {
  
  this->SetFourMomentum( newMomentum );
  this->SetVertex( newVertex );
  _FourMomentumHistory.push_back( _FourMomentum );
  _VertexHistory.push_back( _Vertex );

}
