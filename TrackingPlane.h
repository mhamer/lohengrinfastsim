#include <vector>
#include <TLorentzVector.h>
#include <string>
#include <TH2D.h>

class TrackingPlane {
  public:
    TrackingPlane();
    TrackingPlane( std::string configFileName );
    ~TrackingPlane() { } 
     
    void SetDimX( double );
    void SetDimY( double );
    void SetDimZ (double );
    void SetDimZSensor( double );
    void SetDimZASIC( double );
    void SetPixelPitchX( double );
    void SetPixelPitchY( double );
    void SetMaterial( int );
    void SetXZero( double );
    void SetLocation( double, double, double );
    void AddHitToHitMap( double, double );
    void InitializeHitMap();

    double GetDimX();
    double GetDimY();
    double GetDimZ();
    double GetDimZSensor();
    double GetDimZASIC();
    double GetPixelPitchX();
    double GetPixelPitchY();
    double GetMaterial();
    double GetXZero();
    double GetLocationX();
    double GetLocationY();
    double GetLocationZ();
    TH2D* GetHitMap();

    std::vector<double> GetMSMeanValues( TLorentzVector incidentParticle, double SFz = 1. );

   private:
    double _DimX;         // height
    double _DimY;         // width
    double _DimZ;         // thickness
    double _DimZSensor;   // sensor thickness
    double _DimZASIC;     // readout chip thickness
    double _PixelPitchX;  // pixel pitch in x
    double _PixelPitchY;  // pixel pitch in y
    double _XZero;        // X0 of the material
    double _LocationX;    // Location in the global coordinate system
    double _LocationY;    // Location in the global coordinate system
    double _LocationZ;    // Location in the global coordinate system
    int _Material;        // material: 14: silicon
    TH2D *_trackHits;      // 2D hitmap (incoming hits) 

      
      
};

