#include <iostream>
#include <TStyle.h>
#include <TRandom3.h>
#include <TCanvas.h>
#include "TrackingPlane.h"
#include "Particle.h"

void setStyle(double ytoff = 1.0, bool marker = true, double left_margin = 0.15); 
std::vector<double> CalculateLinePlaneIntersection(Particle, TrackingPlane);
void SimulateParticlePassageThroughTrackerPlane( Particle*, TrackingPlane*, TRandom3*, bool, TH1D*, TH1D*, TH1D*, TH2D*, TH1D*, TH1D* );


int main( int argc, char **argv ) {

  setStyle();
  TRandom3 rand(12345);

  TrackingPlane *tp1 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane1.txt");
  TrackingPlane *tp2 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane2.txt");
  TrackingPlane *tp3 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane3.txt");
  TrackingPlane *tp4 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane4.txt");
  TrackingPlane *tp5 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane5.txt");
  TrackingPlane *tp6 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane6.txt");
  TrackingPlane *tp7 = new TrackingPlane("ConfigFiles/Tracker/ConfigFile_TrackingPlane7.txt");
  
  std::vector<TrackingPlane*> trackingPlanes;
  trackingPlanes.push_back( tp1 );
  trackingPlanes.push_back( tp2 );
  trackingPlanes.push_back( tp3 );
  trackingPlanes.push_back( tp4 );
  trackingPlanes.push_back( tp5 );
  trackingPlanes.push_back( tp6 );
  trackingPlanes.push_back( tp7 );


  TH1D *hThetaP = new TH1D("","", 50, -0.003, 0.003 );
  TH1D *hThetaR = new TH1D("","", 50, 0., .003 );
  TH1D *hOffset = new TH1D("","", 50, 0., .003 );
  TH1D *hOffsetX = new TH1D("","", 50, -0.003, .003 );
  TH1D *hOffsetY = new TH1D("","", 50, -0.003, .003 );
  TH2D *hOT = new TH2D("","", 50, -0.003, 0.003, 50, -0.003, 0.003);

  
  Particle el("ConfigFiles/Particles/ConfigFile_ELSAElectron.txt");
  for( unsigned int i = 0; i < trackingPlanes.size(); ++i ) {
    std::cout << "expected means: " << trackingPlanes.at(i)->GetMSMeanValues(el.GetFourMomentum()).at(0) << " and " << trackingPlanes.at(i)->GetMSMeanValues(el.GetFourMomentum()).at(1) << std::endl;
  }

  for( unsigned int ip = 0; ip < 1.e6; ++ip ) {
  
    Particle el1("ConfigFiles/Particles/ConfigFile_ELSAElectron.txt");
    
    for( unsigned int i = 0; i < trackingPlanes.size(); ++i ) {
      SimulateParticlePassageThroughTrackerPlane( &el1, trackingPlanes.at(i), &rand, false, hThetaP, hThetaR, hOffset, hOT, hOffsetX, hOffsetY );
    }
  }
  TCanvas c("","");
  for( unsigned int i = 0; i < trackingPlanes.size(); ++i ) {
    trackingPlanes.at(i)->GetHitMap()->Draw("COLZ");
    char pdfname[200];
    sprintf(pdfname, "plots/HitMapPlane%i.pdf", i+1);
    c.Print(pdfname);
  }
  gStyle->SetOptStat(1111);
  hThetaP->Draw("HIST");
  c.Print("ThetaPlane.pdf");
  hThetaR->Draw("HIST");
  c.Print("ThetaSolid.pdf");
  hOffset->Draw("HIST");
  c.Print("Offset.pdf");
  hOffsetX->Draw("HIST");
  c.Print("OffsetX.pdf");
  hOffsetY->Draw("HIST");
  c.Print("OffsetY.pdf");
  hOT->Draw("COLZ");
  c.Print("OffsetAngleCorrelation.pdf");
  std::cout << "correlation coefficient is " << hOT->GetCorrelationFactor() << std::endl;
  

}  

void SimulateParticlePassageThroughTrackerPlane( Particle *p, TrackingPlane *t, TRandom3 *rand, bool verbose, TH1D* htp, TH1D* htr, TH1D *hto, TH2D* hot, TH1D *hox, TH1D *hoy ) {

  std::vector<double> is = CalculateLinePlaneIntersection( *p, *t );
  TVector3 isVec( is.at(0), is.at(1), is.at(2) );
  p->SetFourMomentumVertex( p->GetFourMomentum(), isVec );

  t->AddHitToHitMap( is.at(0), is.at(1) );
  double sf = 1.;
  double nomdx = t->GetDimZ()*tan(is.at(3));
  double nomdy = t->GetDimZ()*tan(is.at(4));
  sf = sqrt(t->GetDimZ()*t->GetDimZ()+nomdx*nomdx+nomdy*nomdy)/t->GetDimZ();
  //std::cout << "running with scalefactor " << sf << std::endl;
  std::vector<double>  sigmas = t->GetMSMeanValues( p->GetFourMomentum(), sf);
  double z1 = rand->Gaus(0., 1.);
  double z2 = rand->Gaus(0., 1.);
  double z3 = rand->Gaus(0., 1.);
  double z4 = rand->Gaus(0., 1.);

  double deltaX = z1*t->GetDimZ()/sqrt(12.)*sigmas.at(0) + z2*t->GetDimZ()*sigmas.at(0)/2.;
  double deltaY = z3*t->GetDimZ()/sqrt(12.)*sigmas.at(0) + z4*t->GetDimZ()*sigmas.at(0)/2.;
  double deltaThetaX = 2*M_PI;
  while( fabs(deltaThetaX) > M_PI ) deltaThetaX = z2*sigmas.at(0); 
  double deltaThetaY = 2*M_PI;
  while( fabs(deltaThetaY) > M_PI ) deltaThetaY = z4*sigmas.at(0);

  hot->Fill( deltaX, deltaThetaX );
  hot->Fill( deltaY, deltaThetaY );
  htp->Fill( deltaThetaX );
  htp->Fill( deltaThetaY );
  htr->Fill( sqrt( deltaThetaX*deltaThetaX + deltaThetaY*deltaThetaY );
  hto->Fill( sqrt( deltaX*deltaX + deltaY*deltaY) );
  hox->Fill( deltaX );
  hoy->Fill( deltaY );

  TVector3 outVertex( p->GetVertex().X() + deltaX, p->GetVertex().Y() + deltaY, p->GetVertex().Z() + t->GetDimZ() );
  //std::cout << "new angle in x is " << is.at(3) << " + " << deltaThetaX << std::endl;
  //std::cout << "new angle in y is " << is.at(4) << " + " << deltaThetaY << std::endl;
  double thetaXOut = is.at(3) + deltaThetaX;
  double thetaYOut = is.at(4) + deltaThetaY;
  
  double mass = p->GetFourMomentum().M();
  TLorentzVector newFourMomentum;
  double npx = p->GetFourMomentum().P()*sin(thetaXOut);
  double npy = p->GetFourMomentum().P()*sin(thetaYOut);
  double npz = sqrt(p->GetFourMomentum().P()*p->GetFourMomentum().P() - npx*npx - npy*npy );
  double nE = sqrt(mass*mass + npx*npx + npy*npy + npz*npz);
  newFourMomentum.SetPxPyPzE( npx, npy, npz, nE );
  p->SetFourMomentumVertex(newFourMomentum, outVertex );
  
  if( verbose ) {

    TVector3 vIn1 = p->GetVertexHistory().at(p->GetVertexHistory().size()-2);
    TLorentzVector pIn1 = p->GetFourMomentumHistory().at(p->GetFourMomentumHistory().size()-2);
  
    std::cout << "particle is entering the tracking plane: " << std::endl;
    std::cout << "\t  x = " << vIn1.X() << std::endl;
    std::cout << "\t  y = " << vIn1.Y() << std::endl;
    std::cout << "\t  z = " << vIn1.Z() << std::endl;
    std::cout << "\t px = " << pIn1.Px() << std::endl;
    std::cout << "\t py = " << pIn1.Py() << std::endl;
    std::cout << "\t pz = " << pIn1.Pz() << std::endl;
    std::cout << "\t  E = " << pIn1.E() << std::endl;
    std::cout << "\t  M = " << pIn1.M() << std::endl;
    std::cout << "particle is leaving the tracking plane: " << std::endl;
    std::cout << "\t  x = " << p->GetVertex().X() << std::endl;
    std::cout << "\t  y = " << p->GetVertex().Y() << std::endl;
    std::cout << "\t  z = " << p->GetVertex().Z() << std::endl;
    std::cout << "\t px = " << p->GetFourMomentum().Px() << std::endl;
    std::cout << "\t py = " << p->GetFourMomentum().Py() << std::endl;
    std::cout << "\t pz = " << p->GetFourMomentum().Pz() << std::endl;
    std::cout << "\t  E = " << p->GetFourMomentum().E() << std::endl;
    std::cout << "\t  M = " << p->GetFourMomentum().M() << std::endl;
  }

}


std::vector<double> CalculateLinePlaneIntersection(Particle p, TrackingPlane t) { 

  std::vector<double> returnValues(5,-1.e10);

  TVector3 planeCenter;
  planeCenter.SetXYZ( t.GetLocationX(), t.GetLocationY(), t.GetLocationZ()-t.GetDimZ()/2. );
  
  TVector3 planeV1;
  planeV1.SetXYZ( t.GetLocationX(), t.GetLocationY() - t.GetDimY(), 0. ); // t.GetLocationZ()-t.GetDimZ()/2. );
  
  TVector3 planeV2;
  planeV2.SetXYZ( t.GetLocationX() + t.GetDimX()/2, t.GetLocationY(), 0. ); //t.GetLocationZ()-t.GetDimZ()/2. );
  
  TVector3 planeNormal = planeV1.Cross(planeV2);
  double mag = planeNormal.Mag();
  planeNormal.SetXYZ( planeNormal.X()/mag, planeNormal.Y()/mag, planeNormal.Z()/mag );
 
  TVector3 line;
  line.SetXYZ( p.GetFourMomentum().Px()/p.GetFourMomentum().P(), p.GetFourMomentum().Py()/p.GetFourMomentum().P(), p.GetFourMomentum().Pz()/p.GetFourMomentum().P() );
 
  // choose normal vector such that it points in the same z-direction as the line vector:
  if( planeNormal.Z()*line.Z() < 0. ) {
    //std::cout << " mirroring plane normal vector " << std::endl;
    planeNormal.SetXYZ( -planeNormal.X(), -planeNormal.Y(), -planeNormal.Z() );
  }
  /* 
  std::cout << "normal vector for tracker plane is " << planeNormal.X() << " " << planeNormal.Y() << " " << planeNormal.Z() << std::endl;
  std::cout << "calculated from the two plane vectors: " << std::endl;
  std::cout << planeV1.X() << " " << planeV1.Y() << " " << planeV1.Z() << std::endl;
  std::cout << planeV2.X() << " " << planeV2.Y() << " " << planeV2.Z() << std::endl;
  */
  Double_t norm = line*planeNormal;
  Double_t par = (planeCenter - p.GetVertex())*planeNormal;
  if( norm == 0 ) {
    
    if( par == 0 ) {
      // line is contained in plane
      returnValues.at(0) = 1.e10; 
      returnValues.at(1) = 1.e10;
      returnValues.at(2) = 1.e10;
      returnValues.at(3) = 0.;
      returnValues.at(4) = 0.;
    }
    else {
      // line is parallel but not contained in the plane
      returnValues.at(0) = -1.e10; 
      returnValues.at(1) = -1.e10;
      returnValues.at(2) = -1.e10;
      returnValues.at(3) = 0.;
      returnValues.at(4) = 0.;
    }
  }
  else {
    double d = par/norm;
    returnValues.at(0) = p.GetVertex().X() + d*line.X();
    returnValues.at(1) = p.GetVertex().Y() + d*line.Y();
    returnValues.at(2) = p.GetVertex().Z() + d*line.Z();
    TVector3 lineProjX( line.X(), 0., line.Z() );
    TVector3 normProjX( planeNormal.X(), 0., planeNormal.Z() );
    TVector3 lineProjY( 0., line.Y(), line.Z() );
    TVector3 normProjY( 0., planeNormal.Y(), planeNormal.Z() );
    //std::cout << "calculating angel in x"  << std::endl;
    //std::cout << "projections are : " << lineProjX.X() << " " << lineProjX.Y() << " " << lineProjX.Z() << " vs " << normProjX.X() << " " << normProjX.Y() << " " << normProjX.Z() << " = " << lineProjX*normProjX << " --> cos(thetaX) = " << lineProjX*normProjX/lineProjX.Mag()/normProjX.Mag() << std::endl;
    double thetaX = acos( lineProjX*normProjX/lineProjX.Mag()/normProjX.Mag() );
    double thetaY = acos( lineProjY*normProjY/lineProjY.Mag()/normProjY.Mag() );
    returnValues.at(3) = (lineProjX.X() > 0 ) ? thetaX : -thetaX;
    returnValues.at(4) = (lineProjY.Y() > 0 ) ? thetaY : -thetaY;
  }
  
  return returnValues;

}


void setStyle( double ytoff, bool marker, double left_margin ) {
// use plain black on white colors
Int_t icol=0;
gStyle->SetPalette(1);
gStyle->SetFrameBorderMode(icol);
gStyle->SetCanvasBorderMode(icol);
gStyle->SetPadBorderMode(icol);
gStyle->SetPadColor(icol);
gStyle->SetCanvasColor(icol);
gStyle->SetStatColor(icol);
gStyle->SetTitleFillColor(icol);
// set the paper & margin sizes
gStyle->SetPaperSize(20,26);
gStyle->SetPadTopMargin(0.10);
gStyle->SetPadRightMargin(0.15);
gStyle->SetPadBottomMargin(0.16);
gStyle->SetPadLeftMargin(0.15);
// use large fonts
Int_t font=62;
Double_t tsize=0.04;
gStyle->SetTextFont(font);
gStyle->SetTextSize(tsize);
gStyle->SetLabelFont(font,"x");
gStyle->SetTitleFont(font,"x");
gStyle->SetLabelFont(font,"y");
gStyle->SetTitleFont(font,"y");
gStyle->SetLabelFont(font,"z");
gStyle->SetTitleFont(font,"z");
gStyle->SetLabelSize(tsize,"x");
gStyle->SetTitleSize(tsize,"x");
gStyle->SetLabelSize(tsize,"y");
gStyle->SetTitleSize(tsize,"y");
gStyle->SetLabelSize(tsize,"z");
gStyle->SetTitleSize(tsize,"z");
gStyle->SetTitleBorderSize(0);
//use bold lines and markers
if ( marker ) {
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1.2);
}
gStyle->SetHistLineWidth(Width_t(3.));
// postscript dashes
gStyle->SetLineStyleString(2,"[12 12]");
//gStyle->SetOptStat(0);
gStyle->SetOptFit(1111);
// put tick marks on top and RHS of plots
gStyle->SetPadTickX(1);
gStyle->SetPadTickY(1);
// DLA overrides
gStyle->SetPadLeftMargin(left_margin);
gStyle->SetPadBottomMargin(0.13);
gStyle->SetTitleYOffset(ytoff);
gStyle->SetTitleXOffset(1.0);
gStyle->SetOptTitle(0);
//gStyle->SetStatStyle(0);
//gStyle->SetStatFontSize();
//gStyle->SetStatW(0.17);
//gStyle->SetOptStat(0);
gStyle->SetOptFit(0);
}
