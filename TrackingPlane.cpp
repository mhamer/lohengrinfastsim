#include "TrackingPlane.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include <fstream>
#include <TROOT.h>
#include <TStyle.h>
#include <string>
#include <iostream>

TrackingPlane::TrackingPlane( std::string configFileName ) {

    _DimZSensor = 0.3;
    _DimZASIC = 0.7;
    _DimX = 10;
    _DimY = 10;
    _DimZ = _DimZSensor + _DimZASIC; //0.3+0.7;
    _PixelPitchX = 0.055;
    _PixelPitchY = 0.055;
    _Material = 14;
    _XZero = 93.7;
    _LocationX = 0.;
    _LocationY = 0.;
    _LocationZ = 0.;

    bool userTotalThickness = false;

    std::ifstream configFile( configFileName.c_str(), std::ios::in );
    while( configFile.good() ) {
      std::string key, value;
      configFile >> key >> std::ws >> value;
      if( configFile.eof() ) break;
      std::cout << "read : " << key << " " << value << std::endl;
      if( key == "DIMX"             )  _DimX = atof(value.c_str()); 
      if( key == "DIMY"             )  _DimY = atof(value.c_str()); 
      if( key == "DIMZ"             )  { _DimZ = atof(value.c_str()); userTotalThickness = true; }
      if( key == "DIMZSENSOR"       )  _DimZSensor = atof(value.c_str());
      if( key == "DIMZASIC"         )  _DimZASIC = atof(value.c_str());
      if( key == "PIXELPITCHX"      )  _PixelPitchX = atof(value.c_str()); 
      if( key == "PIXELPITCHY"      )  _PixelPitchY = atof(value.c_str()); 
      if( key == "MATERIAL"         )  _Material = atoi(value.c_str()); 
      if( key == "XZERO"            )  _XZero = atof(value.c_str());
      if( key == "LOCATIONX"        ) _LocationX = atof(value.c_str());
      if( key == "LOCATIONY"        ) _LocationY = atof(value.c_str());
      if( key == "LOCATIONZ"        ) _LocationZ = atof(value.c_str());
    }
    std::cout << "set location to " << _LocationZ << std::endl;
    if( !userTotalThickness ) _DimZ = _DimZSensor + _DimZASIC;
    
    InitializeHitMap();
}

TrackingPlane::TrackingPlane() {

    _DimZSensor = 0.3;
    _DimZASIC = 0.7;
    _DimX = 10;
    _DimY = 10;
    _DimZ = _DimZSensor + _DimZASIC; //0.3+0.7;
    _PixelPitchX = 0.055;
    _PixelPitchY = 0.055;
    _Material = 14;
    _XZero = 93.7;
    _LocationX = 0.;
    _LocationY = 0.;
    _LocationZ = 0.;

    InitializeHitMap();

}

void TrackingPlane::InitializeHitMap() {
  
    _trackHits = new TH2D( "", "", 180, -_DimX/2., _DimX/2., 180, -_DimY/2., _DimY/2. ); 
    _trackHits -> GetXaxis()->SetTitle( "x [mm]" ); 
    _trackHits -> GetYaxis()->SetTitle( "y [mm]" ); 
    _trackHits -> GetZaxis()->SetTitle( "Hits" ); 
    _trackHits -> GetZaxis()->SetTitleOffset( 1.3 ); 

}

void TrackingPlane::SetDimX( double x ) { 
  _DimX = x; 
}

void TrackingPlane::SetDimY( double y ) { 
  _DimY = y; 
}

void TrackingPlane::SetDimZ( double z ) { 
  _DimZ = z; 
}

void TrackingPlane::SetDimZSensor( double z ) { 
  _DimZ -= _DimZSensor; _DimZSensor = z; _DimZ += _DimZSensor; 
}

void TrackingPlane::SetDimZASIC( double z ) { 
  _DimZ -= _DimZASIC; _DimZASIC = z; _DimZ += _DimZASIC; 
}

void TrackingPlane::SetPixelPitchX( double ppx ) { 
  _PixelPitchX = ppx; 
}

void TrackingPlane::SetPixelPitchY( double ppy ) { 
  _PixelPitchY = ppy; 
}

void TrackingPlane::SetMaterial( int m ) { 
  _Material = m; 
}

void TrackingPlane::SetXZero( double xz ) {
  _XZero = xz;
}

void TrackingPlane::SetLocation( double x, double y, double z ) {

  _LocationX = x;
  _LocationY = y;
  _LocationZ = z;

}

double TrackingPlane::GetLocationX() {
  return _LocationX;
}


double TrackingPlane::GetLocationY() {
  return _LocationY;
}

double TrackingPlane::GetLocationZ() {
  return _LocationZ;
}

double TrackingPlane::GetDimX() {
  return _DimX;
}

double TrackingPlane::GetDimY() {
  return _DimY;
}

double TrackingPlane::GetDimZ() {
  return _DimZ;
}

double TrackingPlane::GetDimZSensor() {
  return _DimZSensor;
}

double TrackingPlane::GetDimZASIC() {
  return _DimZASIC;
}

double TrackingPlane::GetPixelPitchX() {
  return _PixelPitchX;
}

double TrackingPlane::GetPixelPitchY() {
  return _PixelPitchY;
}

double TrackingPlane::GetMaterial() {
  return _Material;
}

double TrackingPlane::GetXZero() {
  return _XZero;
}

std::vector<double> TrackingPlane::GetMSMeanValues( TLorentzVector incoming, double SFz ) {
  
  double zl = SFz*_DimZ;

  std::vector<double> returnValues(2,0.);
  returnValues.at(0) = 13.6/(incoming.P()*incoming.Beta())*sqrt(zl/_XZero)*(1.+0.038*log(zl/_XZero));
  returnValues.at(1) = 1./sqrt(3.)*zl*returnValues.at(0);
  return returnValues;
}

TH2D* TrackingPlane::GetHitMap() {
  return _trackHits;
}

void TrackingPlane::AddHitToHitMap( double x, double y) {
  _trackHits->Fill(x, y);
}

