#include <vector>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <string>

class Particle {
  public:
    Particle();
    Particle( std::string configFileName );
    ~Particle() { } 
   
    
    TLorentzVector GetFourMomentum();
    TVector3 GetVertex();
    std::vector<TLorentzVector> GetFourMomentumHistory();
    std::vector<TVector3> GetVertexHistory();

    void SetFourMomentumVertex( TLorentzVector, TVector3 );


  private:
    int _pdgID;
    TLorentzVector _FourMomentum;
    TVector3 _Vertex;
    std::vector<TLorentzVector> _FourMomentumHistory;
    std::vector<TVector3> _VertexHistory;
    
    void SetFourMomentum( TLorentzVector );
    void SetVertex( TVector3 );

      
      
};

